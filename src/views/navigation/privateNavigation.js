import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Link, Switch, Route, Redirect,
} from 'react-router-dom';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';

import Page1 from '../pages/page1';

class PrivateNavigation extends Component {
  componentDidMount() {
    window.onbeforeunload = () => {};
    window.onload = () => {};
  }

  render() {
    return (
      <div className="app">
        <div className="app-body">
          <main className="main">
            <Container fluid>
              <Switch>
                <Route exact path="/page1" name="page1" component={Page1} />
                <Redirect from="/" to="/" />
              </Switch>
            </Container>
          </main>
        </div>
      </div>
    );
  }
}

PrivateNavigation.defaultProps = {

};

PrivateNavigation.propTypes = {
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateNavigation);
