import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import NotFound from '../pages/notFound';
import Login from '../pages/login';
import PrivateNavigation from './privateNavigation';
import PrivateRoute from '../components/privateRoute';

class Navigation extends React.Component {
  render() {
    const { authenticated } = this.props;
    return (
      <main>
        <Router>
          <Switch>
            <Route path="/login" name="login" component={Login} />
            <PrivateRoute path="/" name="full" component={PrivateNavigation} authenticated />
            <Route path="*" name="notFound" component={NotFound} />
          </Switch>
        </Router>
      </main>
    );
  }
}

Navigation.defaultProps = {
  authenticated: false,
};

Navigation.propTypes = {
  authenticated: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    authenticated: state.login.isSuccess,
  };
}
export default connect(mapStateToProps)(Navigation);
