import React, { useState, useEffect } from 'react';
import { Row, Form } from 'reactstrap';
import _ from 'lodash';
import {
  RecoilRoot,
  atom,
  selector,
  useRecoilState,
  useRecoilValue,
  useSetRecoilState,
} from 'recoil';
import { NormalButton, RegularInput, IconInput } from 'component-ui-bioflux';
import icPasword from '../../../assets/images/svg/icon-password.svg';
import icEye from '../../../assets/images/svg/ic_eye.svg';
import recoilState from './recoil';

function LoginHtml(props) {
  const [formData, setFormData] = useRecoilState(recoilState.formDataRecoil);
  const [listError, setListError] = useRecoilState(recoilState.litsErrorsRecoil);
  const onClickSubmitRecoil = useSetRecoilState(recoilState.onClickSubmitRecoil);
  const [passwordType, setPasswordType] = useState();

  const onSubmit = () => {
    onClickSubmitRecoil();
  };

  const onClick = () => {
    onClickSubmitRecoil('hello');
  };

  const onChange = (name, value) => {
    const clone = { ...formData };
    _.assign(clone, { [name]: value });
    setFormData(clone);
  };

  const onChangePasswordType = () => {

  };

  console.log('=== render ===');
  return (
    <div className="login-page">
      <h3>
        Login
      </h3>
      <Form onSubmit={onSubmit} className="login-form-v2">
        <RegularInput
          className="input-box-v2"
          type="text"
          placeholder="Username"
          name="userName"
          onChange={onChange}
          value={formData?.userName}
        />
        <IconInput
          iconComponent={(
            <NormalButton
              isFilled
              className="input-icon-button-v2"
              onClick={onChangePasswordType}
              iconComponent={<img src={passwordType === 'password' ? icPasword : icEye} alt="password" />}
              hasIcon
            />
                )}
          className="input-box-v2"
          type={passwordType}
          placeholder="Password"
          name="passWord"
          onChange={onChange}
          value={formData?.passWord}
        />
        {
                listError?.length > 0
                  ? (
                    <span className="help-block animated fadeIn fastest">
                      Invalid credentials
                    </span>
                  )
                  : (
                    <span className="help-block animated fadeOut fastest" style={{ color: 'transparent' }}>
                      Invalid credentials
                    </span>
                  )
              }
        <Row>
          <NormalButton isLinkButton isTertiary className="forgot-pass" buttonName="Forgot Password?" />
          <NormalButton isLarge isPrimary1 isFilled type="button" onClick={onClick} buttonName="Sign in" className="normal-btn-v2" />
        </Row>
      </Form>
    </div>
  );
}

function Login() {
  return (
    <RecoilRoot>
      <LoginHtml />
    </RecoilRoot>
  );
}

export default Login;
