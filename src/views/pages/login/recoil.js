import {
  atom,
  selector,
  useRecoilState,
  useRecoilValue,
  useRecoilCallback,
  useRecoilValueLoadable,
} from 'recoil';


const formDataRecoil = atom({
  key: 'formDataRecoil',
  default: {
    userName: '',
    passWord: '',
  },
});

const litsErrorsRecoil = atom({
  key: 'litsErrorsRecoil',
  default: [],
});

// const funcOnClick = useRecoilCallback(({ set }) => async (newValue, data) => {
//   console.log('funcOnClick', data, newValue);
//   set(litsErrorsRecoil, [{}]);
//   fetch('https://jsonplaceholder.typicode.com/users').then((result) => {
//     console.log('result', result);
//     set(formDataRecoil, { userName: 'test', passWord: 'test' });
//     set(litsErrorsRecoil, []);
//   });
// });

const funcOnClick = (newValue, data, set) => {
  console.log('funcOnClick', data, newValue);
  set(litsErrorsRecoil, [{}]);
  fetch('https://jsonplaceholder.typicode.com/users').then((result) => {
    console.log('result', result);
    set(formDataRecoil, { userName: 'test', passWord: 'test' });
    set(litsErrorsRecoil, []);
  });
};

const onClickSubmitRecoil = selector({
  key: 'onClickSubmitRecoil',
  set: ({ get, set }) => {
    const data = get(formDataRecoil);
    return newValue => funcOnClick(newValue, data, set);
  },
});

export default {
  formDataRecoil,
  onClickSubmitRecoil,
  litsErrorsRecoil,
};
