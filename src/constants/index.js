export const AppFlowActions = {
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_COMPLETE: 'LOGIN_COMPLETE',

  GET_ALL_DATA_REQUEST: 'GET_ALL_DATA_REQUEST',
  GET_ALL_DATA_COMPLETE: 'GET_ALL_DATA_COMPLETE',

  RELOAD_PAGE_REQUEST: 'RELOAD_PAGE_REQUEST',
};

export const EMITTER_CONSTANTS = {
  LOGOUT_REQUEST: 'LOGOUT_REQUEST',
};
